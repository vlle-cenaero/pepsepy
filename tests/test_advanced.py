# -*- coding: utf-8 -*-

from context import pepsepy

import unittest


class AdvancedTestSuite(unittest.TestCase):
    """Advanced test cases."""

    def test_thoughts(self):
        self.assertIsNone(pepsepy.time_str())
        
if __name__ == '__main__':
    unittest.main()
